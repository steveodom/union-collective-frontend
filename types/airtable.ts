// Photos
export interface IPhotoThumbnail {
  url: string
  width: number
  height: number
}

export interface IThumbnails {
  small: IPhotoThumbnail
  large: IPhotoThumbnail
  full: IPhotoThumbnail
}

export interface IAttachment {
  id: string
  url: string
  filename: string
  size: number
  type: string // 'application/pdf'
  thumbnails: IThumbnails
}

export interface IPhoto {
  id: string
  url: string
  type: string
  filename: string
  thumbnails?: IThumbnails
}

export interface IPhotos extends Array<IPhoto> {}

export interface IScheduleRow {
  id: string
  rowId: number
  parent?: IScheduleRow
  Name: string
  ActionName: string
  token: string
  Category?: Array<string>
  'Base Start Date'?: Date
  startDate?: Date
  endDate?: Date
  'Dependent On': Array<string>
  Status: string
  'Research Days'?: number
  'Bid Gathering Days'?: number
  'Order Delivery Days'?: number
  'Work Days to Complete'?: number
  'Pad Days from Previous'?: number
  Notes?: string
  customClass: string
}

export interface IScheduleObjects {
  [id: string]: IScheduleRow
}

export interface ISchedule extends Array<IScheduleRow> {}

export interface IBid {
  id: string
  Provider: Array<string>
  Amount: number
  Category: Array<string>
  'Cost Group': Array<string>
  Attachment: Array<IAttachment>
}

export interface IBids extends Array<IBid> {}

export interface IProvider {
  id: string
  Company: string
  Name: string
  'Phone Number': string
  Email: string
  // eslint-disable-next-line no-use-before-define
  costs?: ICosts
}

export interface IProviders {
  [id: string]: IProvider
}

export enum CostType {
  ACTUAL = 'Actual',
  ESTIMATE = 'Estimate',
  COMPLETE = 'Estimate Complete'
}

export interface ICost {
  id: string
  Name: string
  Type: CostType
  Amount: number
  Provider: Array<string> | IProvider
  Bids: Array<string>
  'Cost Group': Array<string>
  'Date of Payment': string
  Photos: IPhotos
  Attachments: Array<IAttachment>
  'Budget Line Item': Array<string>
  code?: number | string
  Notes: string
}

export interface ICosts extends Array<ICost> {}

export interface ICostGroup extends ICost {
  categories?: Array<string>
  costs: Array<ICost>
  thumbnail?: string
  total?: number
  code?: number | string
}
export interface ICostGroups {
  [id: string]: ICostGroup
}

export interface ICostGroupArray extends Array<ICostGroup> {}

export interface ICategory {
  id: string
  Name: string
  'Cost Group': ICostGroup
  description?: string
}

export interface ICategories {
  [id: string]: ICategory
}
