import type { ParsedContent } from '@nuxt/content/dist/runtime/types'

export interface InstructionsContent extends ParsedContent {
  title: string
  description: string
  date: Date
}
