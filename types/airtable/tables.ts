import { IAttachment, IPhotos } from '@/types/airtable'

export interface ITableCostRowType {
  id: string
  key: string
  name: string
  amount?: number
  formattedAmount?: string
  date: string
  formattedDate: string
  code: string | number
  costGroup?: string
  invoices: Array<IAttachment>
  Photos?: IPhotos
}

export interface ITableProviderBasedCostRowType extends ITableCostRowType {
  costName: string
}
