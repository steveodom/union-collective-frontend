export interface IGanttRow {
  id: String // row?
  row_id: number // added to show multiple elements in the same row via the package fork
  parent: IGanttRow
  Name: string
  lineName: string
  token: string
  start: Date
  end: Date
  progress: number
  dependencies: string
  custom_class: string
  Category: Array<string>
}

export type GanttStyleOptions = {
  bgColor: string // default: '#fff'
  lineColor: string // default: '#eee'
  redLineColor: string // default: '#f04134'
  groupBack: string // default: '#3db9d3'
  groupFront: string // default: '#299cb4'
  taskBack: string // default: '#65c16f'
  taskFront: string // default: '#46ad51'
  milestone: string // default: '#d33daf'
  warning: string // default: '#faad14'
  danger: string // default: '#f5222d'
  link: string // default: '#ffa011'
  textColor: string // default: '#222'
  lightTextColor: string // default: '#999'
  lineWidth: string // default: '1px'
  thickLineWidth: string // default: '1.4px'
  fontSize: string // default: '14px'
  smallFontSize: string // default: '12px'
  fontFamily: string // default: '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif'
}

export type GanttOptions = {
  viewMode: 'day' | 'week' | 'month'
  onClick: (item: IGanttRow) => {}
  offsetY: number // default: 60,
  rowHeight: number // default: 40,
  barHeight: number // default: 16,
  thickWidth: number // default: 1.4,
  styleOptions: GanttStyleOptions
}
