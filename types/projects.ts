import type { ParsedContent } from '@nuxt/content/dist/runtime/types'

enum BuilderTypeOptions {
  'Designer / Builder',
  'Builder'
}

enum BuildStageType {
  'Design',
  'Permitting',
  'Slab',
  'Frame',
  'Build out',
  'Final Touches',
  'Complete'
}

export interface ProjectContent extends ParsedContent {
  model: string
  title: string
  description: string
  date: Date
  builder: string
  builderType: BuilderTypeOptions
  image: string
  location: string
  stage: BuildStageType
  costs_table_id: string
  schedule_table_id: string
  bids_table_id: string
  providers_table_id: string
  categories_table_id: string
  costs_group_table_id: string
  photos_table_id: string
}
