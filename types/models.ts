import type { ParsedContent } from '@nuxt/content/dist/runtime/types'

enum BuildDifficulty {
  'Easy',
  'Medium',
  'Hard'
}

export interface ModelContent extends ParsedContent {
  id: string
  title: string
  description: string
  provider: string
  date: Date
  footage: number
  image: string
  baths: number
  difficulty: BuildDifficulty
}
