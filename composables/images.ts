import { IAttachment } from '@/types/airtable'

export function useThumbnail(attachment: IAttachment): string {
  return attachment && attachment.thumbnails
    ? attachment.thumbnails.large.url
    : ''
}
