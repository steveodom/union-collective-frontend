import { useBack } from '~/stores/back'

export const useSetBackToHere = () => {
  const { currentRoute } = useRouter()
  const back = useBack()
  back.backTo = currentRoute.value.fullPath
}

export const useGoBack = (defaultUrl) => {
  const back = useBack()
  const backTo = back.backTo
  back.reset()
  navigateTo({ path: (backTo || defaultUrl) as string })
}
