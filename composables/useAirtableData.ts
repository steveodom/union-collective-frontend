import { useAirtable } from '~/stores/airtable'

export const useAirtableData = async () => {
  const project = await useProjectData()
  const airtable = useAirtable()
  await airtable.fetchBids(project.bids_table_id)
  await airtable.fetchCosts(project.costs_table_id)
  await airtable.fetchProviders(project.providers_table_id)
  await airtable.fetchCategories(project.categories_table_id)
  await airtable.fetchCostGroups(project.costs_group_table_id)
  return airtable
}
