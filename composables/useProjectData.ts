import { useRoute } from 'vue-router'
import { useProject } from '~/stores/project'
import { ProjectContent } from '~~/types/projects'

export const useProjectData = async () => {
  const route = useRoute()
  const projectStore = useProject()
  const project = (await projectStore.fetchProject(
    route.params.model as string,
    route.params.id as string
  )) as ProjectContent
  return project
}
