import { Size } from '@/composables/useScreen'

export const useModalWidth = () => {
  const screen = useScreen()
  let width = '630px  '
  switch (screen.current.value) {
    case Size.SMALL:
      width = '100%'
      break
    case Size.MEDIUM:
      width = '100%'
      break
    default:
      return 630
  }
  return width
}
