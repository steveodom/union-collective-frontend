import { Ref, unref } from 'vue'
import type { TableColumnType } from 'ant-design-vue'
import { SortOrder } from 'ant-design-vue/es/table/interface'

import {
  ITableCostRowType,
  ITableProviderBasedCostRowType
} from '@/types/airtable/tables'
import {
  IAttachment,
  ICost,
  ICostGroup,
  ICostGroupArray,
  ICosts,
  IPhotos,
  IProvider
} from '@/types/airtable'
import { toDollars } from '@/utils/str'
import { formatDate } from '@/utils/date'

export const useCostColumns: TableColumnType[] = [
  {
    title: '',
    dataIndex: 'Photos',
    key: 'photos',
    className: 'column-image',
    width: 50
  },
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    sorter: (
      a: ITableProviderBasedCostRowType,
      b: ITableProviderBasedCostRowType
    ) => {
      return ('' + a.name).localeCompare(b.name)
    },
    sortDirections: ['descend', 'ascend']
  },
  {
    title: 'Amount',
    dataIndex: 'formattedAmount',
    key: 'amount',
    sorter: (
      a: ITableProviderBasedCostRowType,
      b: ITableProviderBasedCostRowType
    ) => {
      return a.amount - b.amount
    },
    sortDirections: ['descend', 'ascend'],
    className: 'column-money',
    width: 250
  }
]

export function useProviderBaseCostColumns(
  sortColumn: Ref<string> = ref('name'),
  sortDirection: Ref<SortOrder> = ref('ascend')
): TableColumnType<ITableProviderBasedCostRowType>[] {
  return [
    {
      title: '',
      dataIndex: 'Photos',
      key: 'photos',
      className: 'column-image',
      width: 50
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      ellipsis: true,
      sorter: (
        a: ITableProviderBasedCostRowType,
        b: ITableProviderBasedCostRowType
      ) => {
        return a.name.localeCompare(b.name)
      },
      sortOrder:
        sortColumn.value === 'name' && (sortDirection.value as SortOrder)
    },
    {
      title: 'Amount',
      dataIndex: 'formattedAmount',
      key: 'amount',
      sorter: (
        a: ITableProviderBasedCostRowType,
        b: ITableProviderBasedCostRowType
      ) => {
        return a.amount - b.amount
      },
      sortOrder:
        sortColumn.value === 'amount'
          ? (sortDirection.value as SortOrder)
          : undefined,
      className: 'column-money',
      width: 250
    },
    {
      title: '',
      dataIndex: 'date',
      key: 'date',
      className: 'column-more',
      sorter: (
        a: ITableProviderBasedCostRowType,
        b: ITableProviderBasedCostRowType
      ) => {
        return a?.date?.localeCompare(b?.date)
      },
      sortOrder:
        sortColumn.value === 'date'
          ? (sortDirection.value as SortOrder)
          : undefined,
      width: 50
    }
  ]
}

export function useCostComparisonColumns(
  sortColumn: Ref<string> = ref('name'),
  sortDirection: Ref<SortOrder> = ref('ascend')
): TableColumnType<ITableProviderBasedCostRowType>[] {
  return [
    {
      title: '',
      dataIndex: 'Photos',
      key: 'photos',
      className: 'column-image',
      width: 50
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      ellipsis: true,
      sorter: (
        a: ITableProviderBasedCostRowType,
        b: ITableProviderBasedCostRowType
      ) => {
        return a.name.localeCompare(b.name)
      },
      sortOrder:
        sortColumn.value === 'name'
          ? (sortDirection.value as SortOrder)
          : undefined
    },
    {
      title: 'Amount',
      dataIndex: 'formattedAmount',
      key: 'amount',
      sorter: (
        a: ITableProviderBasedCostRowType,
        b: ITableProviderBasedCostRowType
      ) => {
        return a.amount - b.amount
      },
      sortOrder:
        sortColumn.value === 'amount'
          ? (sortDirection.value as SortOrder)
          : undefined,
      className: 'column-money',
      width: 250
    },
    {
      title: '',
      dataIndex: 'date',
      key: 'date',
      className: 'column-more',
      sorter: (
        a: ITableProviderBasedCostRowType,
        b: ITableProviderBasedCostRowType
      ) => {
        return a?.date?.localeCompare(b?.date)
      },
      sortOrder:
        sortColumn.value === 'date'
          ? (sortDirection.value as SortOrder)
          : undefined,
      width: 50
    }
  ]
}

export function useCostRows(
  costs: ICostGroupArray | ICosts,
  amountField = 'total'
): ITableCostRowType[] {
  return costs
    .map((cost: ICostGroup | ICost) => {
      return {
        id: cost.id,
        key: cost.id,
        name: cost.Name,
        amount: cost[amountField] || 0,
        formattedAmount: toDollars(cost[amountField] || 0),
        date: cost['Date of Payment'],
        formattedDate: formatDate(cost['Date of Payment']),
        code: cost.code,
        costGroup: Array.isArray(cost['Cost Group'])
          ? cost['Cost Group'][0]
          : cost['Cost Group'],
        invoices: unref(cost.Attachments) as Array<IAttachment>,
        Photos: unref(cost.Photos) as IPhotos
      }
    })
    .sort((a, b) => {
      return a.amount > b.amount ? -1 : 1
    })
}

export function useProviderBasedCostRows(
  costs: Array<ICost>,
  amountField = 'total'
): ITableProviderBasedCostRowType[] {
  return costs
    .map((cost: ICost, i: number) => {
      const provider = unref(cost.Provider) as IProvider
      return {
        key: `${provider.id}+${i}`,
        id: provider.id,
        name: provider.Company || provider.Name,
        costName: cost.Name,
        date: cost['Date of Payment'],
        formattedDate: formatDate(cost['Date of Payment']),
        amount: cost[amountField],
        formattedAmount: toDollars(cost[amountField]),
        code: cost.code,
        invoices: unref(cost.Attachments) as IAttachment[],
        Photos: unref(cost.Photos) as IPhotos
      }
    })
    .sort((a, b) => {
      return a.amount > b.amount ? -1 : 1
    })
}

export function useTotalRows(rows: Array<ITableCostRowType>): string {
  let totalCosts = 0

  unref(rows).forEach(({ amount }) => {
    totalCosts += amount || 0
  })
  return toDollars(totalCosts)
}
interface ScrollHeight {
  y: number
}

export function maxHeightTable(): ScrollHeight {
  if (typeof window !== 'undefined') {
    const height = window.innerHeight
    return { y: height - 300 }
  }
}
