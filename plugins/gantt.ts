import Gantt from './gantt/gantt'

export default defineNuxtPlugin((/* nuxtApp */) => {
  return {
    provide: {
      Gantt: () => Gantt
    }
  }
})
