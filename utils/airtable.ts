import Airtable from 'airtable'

export function airtableBase() {
  Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: useRuntimeConfig().airtableKey
  })

  return Airtable.base('appZWcduCJQdggFbk')
}
