// export const iconColors = {
//   default: '',
//   plumbing: 'red',
//   slab: '#b1cee5',
//   electrical: '#f9ad8d',
//   framing: '#ffece3',
//   windows: '#ff6c4a',
//   roofing: '#f9ad8d',
//   hvac: '#ffece3',
//   insulation: '#ff6c4a',
//   sheetrock: '#f9ad8d',
//   'interior-trim': '#ffece3',
//   painting: '#ff6c4a',
//   cabinets: '#f9ad8d',
//   bathrooms: '#ffece3',
//   lighting: '#ff6c4a',
//   flooring: '#f9ad8d',
//   flatwork: '#ffece3',
//   appliances: '#ff6c4a',
//   'exterior-trim': '#f9ad8d'
// }

export function getCategoryFromCode(code: string | number) {
  if (typeof code === 'string') {
    code = parseInt(code)
  }
  switch (code) {
    case 100:
      return 'prelim'
    case 200:
    case 201:
    case 202:
      return 'slab'
    case 300:
      return 'plumbing'
    case 500:
      return 'electrical'
    case 400:
    case 1800:
      return 'framing'
    case 600:
      return 'windows'
    case 700:
      return 'roofing'
    case 800:
      return 'exterior-trim'
    case 801:
      return 'interior-trim'
    case 900:
      return 'hvac'
    case 1000:
      return 'insulation'
    case 1100:
      return 'sheetrock'
    case 1200:
      return 'cabinets'
    case 1300:
      return 'bathrooms'
    case 1400:
      return 'lighting'
    case 1500:
      return 'flooring'
    case 1600:
      return 'painting'
    case 1700:
      return 'appliances'
    default:
      return 'default'
  }
}
