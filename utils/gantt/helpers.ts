import { DateTime } from 'luxon'
import { IScheduleObjects, IScheduleRow } from '@/types/airtable'

export function setInitialStartDate(record: IScheduleRow) {
  const status = record.Status
  const startDate = record['Base Start Date']
  if (status !== 'Base Start Date') {
    throw new Error(
      'The first row should have a status equal to Base Start Date'
    )
  }

  if (!startDate) {
    throw new Error('The first row base start date should be a date')
  }

  return startDate
}

export function calcEndDate(startDate: Date, record: IScheduleRow): Date {
  const {
    'Work Days to Complete': workDays,
    'Pad Days from Previous': padDays
  } = record
  const len = (workDays || 0) + (padDays || 0)
  return addDays(startDate, len)
}

export function addDays(date: Date, days: number): Date {
  return DateTime.fromISO(date).plus({ days }).toISODate()
}

export function findLatestDependency(
  dependentOn: Array<string>,
  ganttOptions: IScheduleObjects
): IScheduleRow {
  const latestDependency = dependentOn.reduce((acc, id) => {
    const dependency = ganttOptions[id]
    if (dependency.endDate && !acc.endDate) {
      return dependency
    }

    if (dependency.endDate > acc.endDate) {
      return dependency
    }
    return acc
  }, ganttOptions[dependentOn[0]])
  return latestDependency
}
