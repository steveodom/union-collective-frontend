import { calcEndDate, setInitialStartDate } from './helpers'
import { ISchedule, IScheduleObjects, IScheduleRow } from '@/types/airtable'

export function rowsToObject(schedule: ISchedule): IScheduleObjects {
  let rowStartDate: Date
  let lastId: string
  const rows = {} as IScheduleObjects
  ;[...schedule].forEach((assumption: IScheduleRow, i: number) => {
    // get our initial start date from the first row.
    if (i === 0) {
      rowStartDate = setInitialStartDate(assumption)
    } else {
      const previousRow = rows[lastId]
      rowStartDate = previousRow.endDate
    }
    const row = Object.assign({}, assumption, {
      rowId: i,
      token: assumption.token,
      startDate: rowStartDate,
      endDate: calcEndDate(rowStartDate, assumption),
      customClass: 'work'
    })
    lastId = assumption.id
    rows[assumption.id] = row
  })
  return rows
}
