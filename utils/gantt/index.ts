import { rowsToObject } from './initial'
import { addDays, calcEndDate, findLatestDependency } from './helpers'
import { ISchedule, IScheduleObjects, IScheduleRow } from '@/types/airtable'
import { IGanttRow } from '@/types/gantt'

export function buildGanntRows(schedule: ISchedule) {
  const rowsObject = rowsToObject(schedule) as IScheduleObjects

  // build start and end date based on dependencies
  Object.keys(rowsObject).forEach((id) => {
    const row = Object.assign({}, rowsObject[id])
    const { 'Dependent On': dependentOn } = row
    if (dependentOn) {
      const latestDependency = findLatestDependency(
        dependentOn,
        rowsObject
      ) as IScheduleRow
      if (latestDependency?.endDate) {
        row.parent = latestDependency
        row.startDate = latestDependency.endDate
        row.endDate = calcEndDate(row.startDate, row)
        // update the row
        rowsObject[id] = row
      }
    }
  })

  Object.keys(rowsObject).forEach((id) => {
    const row = Object.assign({}, rowsObject[id])
    const {
      'Research Days': rd,
      'Bid Gathering Days': bd,
      'Order Delivery Days': od
    } = row
    let order, bid, research, endDate
    if (od && od > 0) {
      endDate = addDays(row.startDate, -1)
      order = Object.assign({}, row, {
        id: `${row.id}-order`,
        ActionName: '&#x1F69A;', // https://www.unicode.org/emoji/charts/full-emoji-list.html#1f69b
        startDate: addDays(endDate, od * -1),
        endDate,
        customClass: 'delivery'
      })
      rowsObject[order.id] = order
    }

    if (bd && bd > 0) {
      const base = order || row
      endDate = addDays(base.startDate, -1)
      bid = Object.assign({}, row, {
        id: `${row.id}-bid`,
        ActionName: 'Bids',
        startDate: addDays(endDate, bd * -1),
        endDate,
        customClass: 'bid'
      })
      rowsObject[bid.id] = bid
    }

    if (rd && rd > 0) {
      const base = bid || order || row
      endDate = addDays(base.startDate, -1)
      research = Object.assign({}, row, {
        id: `${row.id}-research`,
        ActionName: 'Research',
        startDate: addDays(endDate, rd * -1),
        endDate,
        customClass: 'research'
      })
      rowsObject[research.id] = research
    }
  })

  // convert to gantt data format
  const ganttRows: IGanttRow[] = Object.keys(rowsObject).map((id) => {
    const row = rowsObject[id]
    const {
      rowId,
      startDate,
      endDate,
      ActionName,
      parent,
      'Dependent On': relatedIds,
      Category,
      customClass
    } = row
    const dependencies = relatedIds ? relatedIds.map((rid) => rid) : []
    return {
      id,
      row_id: rowId,
      parent,
      start: new Date(startDate),
      end: new Date(endDate),
      name: ActionName,
      lineName: row.Name, // eg: 'Energy Study' instead of 'Research'
      progress: 0,
      dependencies,
      Category,
      custom_class: customClass,
      token: row.token
    }
  })
  return ganttRows
}
