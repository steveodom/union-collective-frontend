import { IPhotos, IPhoto } from '@/types/airtable'

export function photosOnly(photos: IPhotos): IPhoto[] {
  return photos.filter((photo) => {
    return ['image/jpeg', 'image/png'].includes(photo.type)
  })
}

export function photoThumbnail(photos: IPhotos | undefined): string {
  if (photos) {
    const photo = photos.find((p: IPhoto) => {
      return ['image/jpeg', 'image/png'].includes(p.type)
    })
    return photo && photo.thumbnails ? photo.thumbnails.large.url : ''
  } else {
    return ''
  }
}
