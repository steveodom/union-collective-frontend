// capitalize each word in a string
export function capitalize(str: string): string {
  return str.replace(/\w\S*/g, (txt) => {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
  })
}

export function strToSlug(str: string): string {
  return str
    .toLowerCase()
    .replace(/ /g, '-')
    .replace(/[^\w-]+/g, '')
}

const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD'
})

const formatterNoDecimal = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
  maximumFractionDigits: 0
})

export function toDollars(str: string | number = 0, rounded = false): string {
  const num = typeof str === 'string' ? parseFloat(str) : str
  return rounded ? formatterNoDecimal.format(num) : formatter.format(num)
}

export function roundNumber(num, precision) {
  return Math.round(num * Math.pow(10, precision)) / Math.pow(10, precision)
}
