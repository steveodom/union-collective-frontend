export interface INameComponents {
  code: number
  name: string
  category: string
}

export function splitNameIntoDetails(str: string): INameComponents {
  // sample: '100 Preliminary Setup: Electrical Service Activation'
  const [category, name] = str.split(':')
  const [code, ...rest] = category.split(' ')
  return {
    code: parseInt(code),
    category: rest.join(' '),
    name: name.trim()
  }
}
