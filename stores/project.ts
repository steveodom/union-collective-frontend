import { defineStore } from 'pinia'
import { ProjectContent } from '~/types/projects'

export interface IProjectState {
  currentTab: string
  project: ProjectContent
  projectId: string
  bidsTableId: string
  categoriesTableId: string
  costsTableId: string
  costGroupsTableId: string
  isLoading: boolean
  photosTableId: string
  providersTableId: string
  scheduleTableId: string
}

export const useProject = defineStore('project', {
  state: (): IProjectState => ({
    currentTab: 'photos',
    project: null,
    projectId: '',
    bidsTableId: '',
    categoriesTableId: '',
    costsTableId: '',
    costGroupsTableId: '',
    isLoading: false,
    photosTableId: '',
    providersTableId: '',
    scheduleTableId: ''
  }),
  actions: {
    async fetchProject(
      modelId: string,
      projectId: string
    ): Promise<ProjectContent> {
      try {
        if (this.projectId !== projectId) {
          this.project = await queryContent<ProjectContent>()
            .where({ _path: `/projects/${modelId}/${projectId}` })
            .findOne()
          this.projectId = projectId

          this.bidsTableId = this.project.bids_table_id
          this.categoriesTableId = this.project.categories_table_id
          this.costsTableId = this.project.costs_table_id
          this.costGroupsTableId = this.project.costs_group_table_id
          this.photosTableId = this.project.photos_table_id
          this.providersTableId = this.project.providers_table_id
          this.scheduleTableId = this.project.schedule_table_id
          return this.project
        } else {
          return this.project
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    }
  }
})
