import { ICost, ICostGroups, ICosts } from '~~/types/airtable'

export function buildCostsByCategory(costs: ICosts, categories: ICostGroups) {
  costs.forEach((cost: ICost) => {
    const { 'Cost Group': groupArray } = cost
    const groupID = groupArray[0]
    const group = categories[groupID]

    // create the group if it doesn't already exist
    if (group) {
      if (group.costs?.length) {
        // push this cost to the group costs
        group.costs.push(cost)
        // merge this cost item Photos with the group Photos
        if (cost.Photos?.length) {
          group.Photos = [...group.Photos, ...cost.Photos]
        }
        // @ts-ignore - Amount is set to 0 below
        group.total += cost.Amount || 0
      } else {
        // create the group costs array and push this cost to it
        group.costs = [cost]
        group.Photos = Array.isArray(cost.Photos) ? cost.Photos : []
        group.total = cost.Amount || 0
      }
    }
  })
  return Object.values(categories)
}
