import { defineStore } from 'pinia'
import { buildCostsByCategory } from './helpers'
import { photoThumbnail } from '~/utils/image'
import { formatDate } from '~/utils/date'

import {
  CostType,
  IBids,
  ICategories,
  ICost,
  ICosts,
  ICostGroup,
  ICostGroups,
  ICostGroupArray,
  IPhotos,
  IProvider,
  IProviders,
  ISchedule,
  ICategory
} from '@/types/airtable'

export interface IAirtableState {
  bids: IBids
  bidsTableId: string
  categories: ICategories
  categoriesTableId: string
  costs: ICosts
  costsTableId: string
  costGroups: ICostGroups
  costGroupsTableId: string
  photos: IPhotos
  photosTableId: string
  providers: IProviders
  providersTableId: string
  schedule: ISchedule
  scheduleTableId: string
}

export const useAirtable = defineStore('airtable', {
  state: (): IAirtableState => ({
    bids: [],
    bidsTableId: '',
    categories: {},
    categoriesTableId: '',
    costGroups: {},
    costGroupsTableId: '',
    costs: [],
    costsTableId: '',
    photos: [],
    photosTableId: '',
    providers: {},
    providersTableId: '',
    schedule: [],
    scheduleTableId: ''
  }),
  actions: {
    async fetchBids(tableID: string) {
      try {
        if (this.bidsTableId !== tableID) {
          this.bids = await $fetch<IBids>('/api/airtable/bids', {
            params: { tableID }
          })
          this.bidsTableId = tableID
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    },
    async fetchCategories(tableID: string) {
      try {
        if (this.categoriesTableId !== tableID) {
          this.categories = await $fetch<ICategories>(
            '/api/airtable/categories',
            {
              params: { tableID }
            }
          )
          this.categoriesTableId = tableID
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    },
    async fetchCostGroups(tableID: string) {
      try {
        if (this.costGroupsTableId !== tableID) {
          this.costGroups = await $fetch<ICategories>(
            '/api/airtable/costgroups',
            {
              params: { tableID }
            }
          )
          this.costGroupsTableId = tableID
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    },
    async fetchCosts(tableID: string) {
      try {
        if (this.costsTableId !== tableID) {
          this.costs = await $fetch<ICosts>('/api/airtable/costs', {
            params: { tableID }
          })
          this.costsTableId = tableID
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    },
    async fetchProviders(tableID: string) {
      try {
        if (this.providersTableId !== tableID) {
          this.providers = await $fetch<IProviders>('/api/airtable/providers', {
            params: { tableID }
          })
          this.providersTableId = tableID
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    },
    async fetchSchedule(tableID: string) {
      try {
        if (this.scheduleTableId !== tableID) {
          this.schedule = await $fetch<ISchedule>('/api/airtable/schedule', {
            params: { tableID }
          })
          this.scheduleTableId = tableID
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    }
  },
  getters: {
    getBidsByCategory:
      (state) =>
      (categoryIDs: Array<string>): IBids => {
        const catIds = Object.values(categoryIDs)
        return state.bids.filter((bid) => {
          const bidCats = bid.Category || []
          const found = bidCats.filter((x) => catIds.includes(x))
          return found.length > 0
        })
      },
    getBidsByCostGroup:
      (state) =>
      (ids: Array<string>): IBids => {
        const catIds = Object.values(ids)
        return state.bids.filter((bid) => {
          const bidCats = bid['Cost Group'] || []
          const found = bidCats.filter((x) => catIds.includes(x))
          return found.length > 0
        })
      },
    getActualCosts(state): ICosts {
      return state.costs.filter((cost: ICost) => {
        return cost.Type === CostType.ACTUAL
      })
    },
    getBudgetedCosts(state): ICosts {
      return state.costs.filter((cost: ICost) => {
        return cost.Type === CostType.ESTIMATE
      })
    },
    getCostsByCategory:
      (state) =>
      (categoryID): ICosts => {
        return state.costs.filter((cost: ICost) => {
          return cost['Budget Line Item'].includes(categoryID)
        })
      },
    getCostsByProvider(state): IProvider[] {
      const providers = JSON.parse(JSON.stringify(state.providers))
      this.getActualCosts.forEach((cost: ICost) => {
        const provider = cost.Provider && providers[cost.Provider as string]
        if (provider) {
          if (!provider.costs) {
            provider.costs = []
          }
          provider.costs.push(cost)
        }
      })
      return Object.values(providers)
    },
    getBudgetedCostsByGroup(state): ICostGroupArray {
      const topLevelCategories = JSON.parse(JSON.stringify(state.costGroups))
      return buildCostsByCategory(this.getBudgetedCosts, topLevelCategories)
    },
    getActualCostsByGroup(state): ICostGroupArray {
      const topLevelCategories = JSON.parse(JSON.stringify(state.costGroups))
      return buildCostsByCategory(this.getActualCosts, topLevelCategories)
    },
    getCostsbyProvider:
      (state) =>
      (providerID): ICosts => {
        return state.costs.filter((cost) => {
          if (cost.Provider) {
            return (
              cost.Type === CostType.ACTUAL && cost.Provider[0] === providerID
            )
          } else {
            return false
          }
        })
      },
    getGroupCosts:
      (state) =>
      (groupId: string): ICostGroup => {
        // @ts-ignore - state is used instead of `this` since we're inside an arrow function
        const group = state.getActualCostsByGroup.find(
          (group) => group.id === groupId
        ) as ICostGroup
        const { costs, ...rest } = group
        const hydratedCosts = costs.map((cost: ICost) => {
          const { Provider: providerArray } = cost
          let provider = { id: '', Name: 'Name not found' }
          if (providerArray instanceof Array && providerArray.length) {
            const providerID = providerArray[0]
            provider = state.providers[providerID] as IProvider
          }
          return Object.assign({}, cost, {
            Provider: provider
          })
        })
        return Object.assign({}, rest, { costs: hydratedCosts })
      },
    getPhotos(): ICostGroupArray {
      const costsWithPhotos = this.getActualCostsByGroup.filter(
        (cost: ICostGroup) => cost?.Photos?.length > 0
      ) as ICostGroupArray
      return costsWithPhotos.map((cost: ICostGroup) => {
        return Object.assign({}, cost, {
          thumbnail: cost.Photos ? photoThumbnail(cost.Photos) : '',
          formatedDate: formatDate(cost['Date of Payment'])
        })
      })
    },
    getSchedule(state): ISchedule {
      return state.schedule
    },
    getCategoryByID:
      (state) =>
      (id: string): ICategory => {
        return state.categories[id]
      },
    getCostGroupByID:
      (state) =>
      (id: string): ICostGroup => {
        return state.costGroups[id]
      },
    getProvider:
      (state) =>
      (providerID: string): IProvider => {
        return state.providers[providerID]
      }
  }
})
