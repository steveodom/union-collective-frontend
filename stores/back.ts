import { defineStore } from 'pinia'

export interface IBackState {
  backTo: string | null
}

export const useBack = defineStore('back', {
  state: (): IBackState => ({
    backTo: null
  }),
  actions: {
    reset() {
      this.backTo = null
    }
  }
})
