import { airtableBase } from '~/utils/airtable'
import { IBid, IBids } from '@/types/airtable'
const base = airtableBase()

async function getData(tableID: string): Promise<IBids> {
  const records = await base(tableID)
    .select({
      fields: ['Provider', 'Amount', 'Category', 'Attachment', 'Cost Group']
    })
    .all()

  const rows: IBids = []
  records.forEach(function (res) {
    const record = res._rawJson.fields as IBid
    record.id = res._rawJson.ID
    rows.push(record)
  })
  return rows
}

export default defineEventHandler((event) => {
  const { tableID } = getQuery(event)
  if (tableID) {
    return getData(tableID as string)
  }
})
