import { defineEventHandler } from 'h3'
import { airtableBase } from '~/utils/airtable'
import { ISchedule, IScheduleRow } from '@/types/airtable'

const base = airtableBase()

async function getData(tableID: string): Promise<ISchedule> {
  const records = await base(tableID)
    .select({
      fields: [
        'Name',
        'ActionName',
        'token',
        'Category',
        'Base Start Date',
        'Dependent On',
        'Status',
        'Research Days',
        'Bid Gathering Days',
        'Order Delivery Days',
        'Work Days to Complete',
        'Pad Days from Previous'
      ],
      view: 'Grid view'
    })
    .all()

  const rows: ISchedule = []
  records.forEach(function (res) {
    const record = res._rawJson.fields as IScheduleRow
    record.id = res._rawJson.id
    rows.push(record)
  })
  return rows
}

export default defineEventHandler((event) => {
  const { tableID } = getQuery(event)
  if (tableID) {
    return getData(tableID as string)
  }
})
