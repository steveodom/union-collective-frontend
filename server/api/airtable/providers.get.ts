import { airtableBase } from '~/utils/airtable'
import { IProvider, IProviders } from '@/types/airtable'
const base = airtableBase()

async function getData(tableID: string): Promise<IProviders> {
  const records = await base(tableID)
    .select({
      fields: ['Company', 'Name', 'Phone Number', 'Email']
    })
    .all()

  const obj: IProviders = {}
  records.forEach(function (res) {
    const record = res._rawJson.fields as IProvider
    const id = res._rawJson.id
    record.id = id
    obj[id] = record
  })
  return obj
}

export default defineEventHandler((event) => {
  const { tableID } = getQuery(event)
  if (tableID) {
    return getData(tableID as string)
  }
})
