import { defineEventHandler } from 'h3'
import { airtableBase } from '~/utils/airtable'
import { ICosts, ICost } from '@/types/airtable'
const base = airtableBase()

async function getData(tableID: string): Promise<ICosts> {
  const records = await base(tableID)
    .select({
      fields: [
        'Name',
        'Amount',
        'Type',
        'Provider',
        'Bids',
        'Budget Line Item',
        'Cost Group',
        'Date of Payment',
        'Attachments',
        'Photos',
        'Notes'
      ]
    })
    .all()

  const items: ICosts = []
  records.forEach(function (res) {
    const record = res._rawJson.fields as ICost
    const id = res._rawJson.id
    record.id = id
    items.push(record)
  })
  return items
}

export default defineEventHandler((event) => {
  const { tableID } = getQuery(event)
  if (tableID) {
    return getData(tableID as string)
  }
})
