import { airtableBase } from '~/utils/airtable'
import { ICategory, ICategories } from '@/types/airtable'
const base = airtableBase()

async function getData(tableID: string): Promise<ICategories> {
  const records = await base(tableID)
    .select({
      fields: ['Name', 'Cost Group', 'Description']
    })
    .all()

  const obj: ICategories = {}
  records.forEach(function (res) {
    const record = res._rawJson.fields as ICategory
    const id = res._rawJson.id
    record.id = id
    obj[id] = record
  })
  return obj
}

export default defineEventHandler((event) => {
  const { tableID } = getQuery(event)
  if (tableID) {
    return getData(tableID as string)
  }
})
