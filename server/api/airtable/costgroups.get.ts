import { airtableBase } from '~/utils/airtable'
import { ICostGroup, ICostGroups } from '@/types/airtable'

const base = airtableBase()

async function getData(tableID: string): Promise<ICostGroups> {
  const records = await base(tableID)
    .select({
      fields: ['Name', 'Categories']
    })
    .all()
  const obj: ICostGroups = {}
  records.forEach(function (res) {
    const record = res._rawJson.fields as ICostGroup
    // split first space into code and rest into a string
    const [code, ...rest] = record.Name.split(' ')
    const name = rest.join(' ')
    const id = res._rawJson.id
    // convert string into number
    record.Name = name
    record.code = parseInt(code)
    record.id = id
    obj[id] = record
  })
  return obj
}

export default defineEventHandler((event) => {
  const { tableID } = getQuery(event)
  if (tableID) {
    return getData(tableID as string)
  }
})
