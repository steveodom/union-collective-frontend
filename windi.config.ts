import { defineConfig } from 'windicss/helpers'
import type { Plugin } from 'windicss/types/interfaces'

// colors
// import colors from 'windicss/colors'

// plugins
import TypographyPlugin from 'windicss/plugin/typography'
import AspectRatioPlugin from 'windicss/plugin/aspect-ratio'
import FiltersPlugin from 'windicss/plugin/filters'

const MyTheme = {
  colors: {
    blue: {
      '100': '#eaf2f8',
      '200': '#b1cee5'
    },
    orange: {
      '100': '#f9ad8d',
      '200': '#ffece3',
      '300': '#ff6c4a'
    }
  }
}

export default defineConfig({
  darkMode: 'class',
  attributify: false,
  extract: {
    include: [
      './components/**/*.{vue,js}',
      './composables/**/*.{js,ts}',
      './layouts/**/*.vue',
      './pages/**/*.vue',
      './plugins/**/*.{js,ts}',
      './utils/**/*.{js,ts}',
      './app.vue'
    ]
  },
  theme: {
    extend: {
      maxWidth: {
        '8xl': '90rem'
      },
      colors: {
        black: 'rgba(39,39,39,1)',
        primary: MyTheme.colors.blue,
        blue: MyTheme.colors.blue,
        gray: {
          100: 'rgba(255, 255, 255, 1)'
        }
      },
      transitionTimingFunction: {
        'in-out': 'cubic-bezier(0.4, 0, 0.2, 1)'
      }
    }
  },
  shortcuts: {
    'light-img': 'block dark:hidden',
    'dark-img': 'hidden dark:block'
  },
  plugins: [
    // filters plugin require for navbar blur
    FiltersPlugin as Plugin,
    TypographyPlugin as Plugin,
    AspectRatioPlugin as Plugin
  ] as Plugin[]
})
