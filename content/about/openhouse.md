---
title: "Welcome"
description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
---

**Openhouse** is a community of individuals coming together to build homes for themselves.

It is inspired by open source software. [Read our manifesto](/about/manifesto).

The results are better built houses at lower prices.

---

### How it works

Building a home is mostly project management - finding, scheduling and managing sub-contractors.

Knowledge is needed on where to start, how to get a building permit, what to look for in contractors, what materials to buy and so on. That is where our community and pre-packaged models come in.

It is very helpful for the same house or ADU be built multiple times by multiple people. The more times the same model house is built -- and that knowledge shared between the builders -- the easier and more efficient each subsequent build should be.