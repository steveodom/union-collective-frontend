---
id: 'little-river'
title: 'Little River'
description: 'First model'
designer: 'Steve Odom'
date: '2022-06-29'
footage: '1295'
baths: 2
difficulty: 'Medium'
image: https://drive.google.com/uc?id=1eGxvDP3aFnRQkcjgU1L62E6tChHwkUKp
---
Modern farmhouse, European inspired design with flexible layout options. The slab footprint is 1450 sq ft. It can be built out to use all of the slab area as enclosed living space or include integrated patios.