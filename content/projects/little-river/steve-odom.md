---
id: 'steve-odom'
model: 'little-river'
title: 'Lockhart, Texas'
description: 'Hello !!!, this is a post about hello world.'
date: '2022-06-29'
builder: 'Steve Odom'
builderType: 'Designer / Builder'
location: 'Lockhart Texas'
stage: 95
baths: 2
footage: 1295
image: https://drive.google.com/uc?id=1rwAYWJcvh2tclmKECQr2x5xZP3EFT6VP
# TODO: figure out how to fetch these based on airtable api key
costs_table_id: 'tblsY9w92AvO7JfAH'
schedule_table_id: 'tblUlqNKlhwc8sc5z'
bids_table_id: 'tblvwnPsbuGtLHF1j'
providers_table_id: 'tblhi71ZpaTkmiF3z'
categories_table_id: 'tbln5GHUurAqASThm'
costs_group_table_id: 'tblEnwkOiQkWxMFpc'
photos_table_id: 'tblsY9w92AvO7JfAH'
---
This build in Lockhart, Texas is situated on the property to take advantage of ample wide open spaces and the herd of deer that roam the property and surrounding flood plain.

![Image](https://drive.google.com/uc?id=1V75vsvTYsTKYjjTSoVxHbyVCUdsAHm2w)

Inspired in part by the Brown shingle houses of Berkely, California. In the book "[_Shingle Style: Living in San Francisco’s Brown Shingles_](http://www.indiebound.org/book/9780847840045)", the author [describes these houses](https://www.berkeleyside.org/2013/08/07/berkeleys-brown-shingles-the-original-party-houses) as:

> A hundred years ago, living in a house made of unpainted redwood shingles was an expression of reverence for the forest. It was a way to live in a redwood tree, literally and metaphorically. The antithesis of the painted Victorians which preceded and surrounded them, brown shingles were close to nature, and often heavily planted up to become their own urban forests.
