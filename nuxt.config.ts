import UnpluginComponentsVite from 'unplugin-vue-components/vite'
import IconsResolver from 'unplugin-icons/resolver'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'
import eslint from 'vite-plugin-eslint'
import en from './locales/en.json'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({
  // server side rendering mode
  ssr: true,

  // app
  app: {
    // global transition
    pageTransition: { name: 'page', mode: 'out-in' },
    layoutTransition: { name: 'layout', mode: 'out-in' }
  },

  // css
  css: [
    'virtual:windi-base.css',
    'virtual:windi-components.css',
    'virtual:windi-utilities.css',
    '~/assets/css/app.styl'
  ],

  runtimeConfig: {
    // The private keys which are only available within server-side
    airtableKey: process.env.AIRTABLE_API_KEY,
    // Keys within public, will be also exposed to the client-side
    public: {
      apiBase: '/api'
    }
  },

  // build
  build: {
    transpile: ['@headlessui/vue']
  },

  // build modules
  buildModules: [], // needed for windicss
  watch: [], // needed for windicss

  // modules
  modules: [
    'nuxt-windicss',
    '@nuxtjs/i18n',
    '@nuxt/content',
    'unplugin-icons/nuxt',
    '@pinia/nuxt',
    '@vueuse/nuxt'
    // '@nuxtjs/svg',
    // '@nuxtjs/style-resources' // allows for importing of styles in style tags
  ],
  // experimental features
  experimental: {
    reactivityTransform: false
  },

  // auto import components
  components: true,

  typescript: {
    // strict: true,
    // typeCheck: true,
    tsConfig: {
      include: ['@yeger/vue-masonry-wall']
    }
  },

  // vite plugins
  vite: {
    plugins: [
      eslint(),
      UnpluginComponentsVite({
        dts: true,
        resolvers: [
          AntDesignVueResolver(),
          IconsResolver({
            prefix: 'Icon'
          })
        ]
      })
    ],
    ssr: {
      noExternal: ['compute-scroll-into-view', 'ant-design-vue']
    }
  },

  i18n: {
    // add `vueI18n` option to `@nuxtjs/i18n` module options
    vueI18n: {
      legacy: false,
      locale: 'en',
      messages: {
        en
      }
    }
  },
  // vueuse
  vueuse: {
    ssrHandlers: true
  },

  // windicss
  windicss: {
    analyze: {
      analysis: {
        interpretUtilities: false
      },
      server: {
        port: 4000,
        open: false
      }
    },
    scan: true
  },

  // content
  content: {
    base: 'content',
    documentDriven: true,
    markdown: {
      mdc: true
    },
    highlight: {
      theme: 'github-dark'
    }
  }
})
